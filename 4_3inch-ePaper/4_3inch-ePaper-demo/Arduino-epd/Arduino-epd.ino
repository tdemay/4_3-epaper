#include <epd.h>

/*********************************************************************************************************
*
* File                : Arduino-epd
* Hardware Environment: 
* Build Environment   : Arduino
* Version             : V1.6.1
* By                  : WaveShare
*
*                                  (c) Copyright 2005-2015, WaveShare
*                                       http://www.waveshare.net
*                                       http://www.waveshare.com
*                                          All Rights Reserved
*
*********************************************************************************************************/
#include <epd.h>



const int led = 13;                           //user led

void setup(void)
{
  /*
  user led init
  */
  pinMode(led, OUTPUT);
  digitalWrite(led, LOW);
  
  epd_init();
  epd_wakeup();
  epd_screen_rotation(EPD_INVERSION);
  epd_set_memory(MEM_TF);
}

void loop(void)
{
  epd_clear();
  
  epd_disp_bitmap("IMGT.BMP", 0, 0);
  epd_udpate();

  epd_enter_stopmode();

}


