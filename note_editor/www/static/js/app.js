var noteEditor = angular.module('note_editor', [
    'common.fabric',
    'common.fabric.utilities',
    'common.fabric.constants',
    'angularify.semantic',
    'angularify.semantic.dropdown'
]);

noteEditor.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');
});

noteEditor.controller('EditorCtrl', ['$scope', 'Fabric', 'FabricConstants', 'Keypress', function($scope, Fabric, FabricConstants, Keypress) {

	$scope.fabric = {};
	$scope.FabricConstants = FabricConstants;

	//
	// Creating Canvas Objects
	// ================================================================
	$scope.addShape = function(path) {
	    console.log("addShape");
	    //	    $scope.fabric.addShape('http://fabricjs.com/assets/15.svg');
	};

	$scope.addImage = function(image) {
	    console.log("addImage");
	    $scope.fabric.addImage('http://stargate-sg1-solutions.com/blog/wp-content/uploads/2007/08/daniel-season-nine.jpg');
	};

	$scope.addText = function() {
		$('#font').dropdown();
	};

	$scope.addImageUpload = function(data) {
	    console.log("addImageUpload") ;
	    var obj = angular.fromJson(data);
	    $scope.addImage(obj.filename);
	};

	//
	// Editing Canvas Size
	// ================================================================
	$scope.selectCanvas = function() {
	    $scope.canvasCopy = {
		width: $scope.fabric.canvasOriginalWidth,
		height: $scope.fabric.canvasOriginalHeight
	    };
	};

	$scope.setCanvasSize = function() {
	    $scope.fabric.setCanvasSize($scope.canvasCopy.width, $scope.canvasCopy.height);
	    $scope.fabric.setDirty(true);
	    delete $scope.canvasCopy;
	};

	//
	// Init
	// ================================================================
	$scope.init = function() {
		console.log("init");
		$scope.fabric = new Fabric({
		JSONExportProperties: FabricConstants.JSONExportProperties,
		textDefaults: FabricConstants.textDefaults,
		shapeDefaults: FabricConstants.shapeDefaults,
		downloadMultipler: 1,
		json: {}
		});
		$scope.fabric.setCanvasSize(800, 600);
		$scope.fabric.setDirty(true);
	};

	$scope.$on('canvas:created', $scope.init);

	$scope.$on('#font:load', function() {
		$('#font').dropdown();
	});

	Keypress.onSave(function() {
	    $scope.updatePage();
	});

    }]);
