from django.shortcuts import render

from django.http import HttpResponse, JsonResponse

# Create your views here.

def index(request):
    return render(request, 'layout.html')


def api_index(request):
    return JsonResponse({'version': '0.1a'})
